@extends('layouts.app')

@section('content')

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Authentification</h1>
                                </div>

                                <form class="user" method="POST" action="{{ url('/login') }}">
                                    @csrf
                                    @if ($errors->has('email'))
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" value="{{ old('email') }}" required placeholder="Email">
                                            <span>{{ $errors->first('email') }}</span>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" value="{{ old('email') }}" required aria-describedby="emailHelp" placeholder="Email">
                                        </div>
                                    @endif

                                    @if ($errors->has('password'))
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                            <span>{{ $errors->first('password') }}</span>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" class="custom-control-input" id="customCheck">
                                            <label class="custom-control-label" for="customCheck">Rester connecté</label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">Connexion</button>
                                    <hr>
                                </form>

                                <div class="text-center">
                                    <a class="small" href="{{route('password.request')}}">Passe oublié ?</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="{{route('register')}}">Nouveau ! S'inscrire.</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<!-- Bootstrap core JavaScript-->
<script src="sm/vendor/jquery/jquery.min.js"></script>
<script src="sm/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="sm/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Custom scripts for all pages-->
<script src="sm/js/sb-admin-2.min.js"></script>

@endsection
