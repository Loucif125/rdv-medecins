@extends('layouts.app')

@section('content')


    <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous">

    </script>


    @if(session('success'))
        <div class="alert alert-success" style="text-align: center" id="success-alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong><i class="fa fa-check"></i> {{session('success')}}</strong>
        </div>

        <script>
            $(document).ready(function() {
                $("#success-alert").hide();

                $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#success-alert").slideUp(500);
                });

            });
        </script>
    @endif


    <div id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">



            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Add rdv--->
                    @if(isset($clients))
                        <div class="containrer-fluid">
                            <div class="container">
                                <!-- Outer Row -->
                                <div class="row justify-content-center">
                                    <div class="col-xl-10 col-lg-12 col-md-9">
                                        <div class="card o-hidden border-0 shadow-lg my-3">
                                            <div class="card-body p-0">
                                                <!-- Nested Row within Card Body -->
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="p-4">
                                                            <div class="text-center">
                                                                <h1 class="h4 text-gray-900 mb-4"><i class="fa fa-clock"></i> Rendez-vous Le :<small>({{$date}})</small> à <small>({{$creneau->HDEBUT.'h'.$creneau->MDEBUT .' - '.$creneau->HFIN.'h'.$creneau->MFIN}})</small></h1>
                                                            </div>
                                                            <form class="user" method="post" action="{{ url('/home/rdv/store') }}">
                                                                @csrf
                                                                <div class="form-group row">
                                                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                                                        <b><i class="fa fa-user-md"></i> Médecin :</b> {{ucwords($medecin->NOM .' '.$medecin->PRENOM)}}
                                                                    </div>

                                                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                                                        <i class="fa fa-user"></i> <b>Client :</b>
                                                                    </div>

                                                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                                                        <select name="id_client" class="form-control">
                                                                            @foreach($clients as $client)
                                                                                <option value="{{$client->ID}}">{{ucwords($client->TITRE.' '.$client->NOM.' '.$client->PRENOM )}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="date" value="{{$date}}" />
                                                                <input type="hidden" name="id_medecin" value="{{$medecin->ID}}" />
                                                                <input type="hidden" name="id_creneau" value="{{$creneau->ID}}" />


                                                                <button class="btn btn-secondary btn-block"> <i class="fa fa-check"></i> Valider </button>
                                                                @if ($errors->any())
                                                                            @foreach ($errors->all() as $error)
                                                                        <span style="color: red"><i class="fa fa-info"></i> {{ $error }}</span>
                                                                            @endforeach
                                                                @endif
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                <!-- Fin Add rdv -->




</div>



                </div>
                <!-- End of Main Content -->




                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Média Social</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

    </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>


@endsection
