@extends('layouts.app')

@section('content')

<script
src="https://code.jquery.com/jquery-3.4.1.js"
integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
crossorigin="anonymous">
</script>

@if(session('success') or isset($success))
<div class="alert alert-success" style="text-align: center" id="success-alert">
<button type="button" class="close" data-dismiss="alert">x</button>
<strong><i class="fa fa-check"></i> {{session('success')}}</strong>
</div>

<script>
$(document).ready(function() {
$("#success-alert").hide();

$("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
$("#success-alert").slideUp(500);
});
});
</script>
@endif


<div id="page-top">
<!-- Page Wrapper -->
<div id="wrapper">

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">
<!-- Begin Page Content -->
<div class="container-fluid mt-2">
<!-- Content Row -->
<div class="row">
<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4 ">
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">User</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"> {{(isset($nbUser) ? $nbUser : "00")}}

                    <button type="button" data-toggle="modal" data-target="#infos" class="btn btn-primary btn-sm"><i class="fa fa-clipboard-list"></i> Liste</button>
                </div>

            </div>
            <div class="col-auto">
                <i class="fas fa-users fa-2x"></i>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
<div class="card border-left-success shadow h-100 py-2">
    <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Médecin(s)</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{(isset($nbMedecin) ? $nbMedecin : "00")}}</div>
            </div>
            <div class="col-auto">
                <i class="fas fa-user-md fa-2x"></i>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
<div class="card border-left-info shadow h-100 py-2">
    <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Client(s)</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{(isset($nbClient) ? $nbClient : "00")}}</div>
            </div>
            <div class="col-auto">
                <i class="fas fa-clipboard-list fa-2x"></i>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Pending Requests Card Example -->
<div class="col-xl-3 col-md-6 mb-4">
<div class="card border-left-warning shadow h-100 py-2">
    <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Nouveaux Client</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{(isset($nbClient) ? $nbClient : "00")}}</div>
            </div>
            <div class="col-auto">
                <i class="fas fa-user fa-2x"></i>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<!-- Content Row -->


</div>
<!-- /.container-fluid -->

<!-- Add Medecin--->
<div class="containrer-fluid">
<div class="container">
<!-- Outer Row -->
<div class="row justify-content-center">
<div class="col-xl-10 col-lg-12 col-md-9">
    <div class="card o-hidden border-0 shadow-lg my-3">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-4">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4"> <i class="fa fa-user-md"></i> Ajouter un médecin :</h1>
                        </div>
        <form class="user" method="post" action="{{ url('/home/medecin/store') }}">
                            @csrf
                            <div class="form-group row">
                                <div class="col-sm-4 mb-3 mb-sm-0">
                                    <select name="titre" class="form-control">
                                        <option value="Mr" selected> Mr</option>
                                        <option value="Mme"> Mme</option>
                                        <option value="Melle"> Melle</option>
                                    </select>
                                </div>

                                @if ($errors->has('nom'))
                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                        <input type="text" name="nom" class="form-control" placeholder="Nom">
                                        <span>{{ $errors->first('nom') }}</span>
                                    </div>
                                @else
                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                        <input type="text" name="nom" class="form-control"  placeholder="Nom">
                                    </div>

                                @endif
                                @if ($errors->has('prenom'))
                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                        <input type="text" name="prenom" class="form-control" placeholder="Prénom">
                                        <span>{{ $errors->first('prenom') }}</span>
                                    </div>
                                @else
                                    <div class="col-sm-4 mb-3 mb-sm-0">
                                        <input type="text" name="prenom" class="form-control" placeholder="Prénom">
                                    </div>
                                @endif
                            </div>

                            <button class="btn btn-primary btn-user btn-block"> <i class="fa fa-plus"></i> Ajouter </button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

</div>

</div>
<!-- Fin add Medecins -->


</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
<div class="container my-auto">
<div class="copyright text-center my-auto">
<span>Copyright &copy; Média Social</span>
</div>
</div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>


<!-- Modal-->
@if(isset($users))
<div class="modal" id="infos">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title"><i class="fa fa-users"></i> Liste des utilisateurs</h4>
<button type="button" class="close" data-dismiss="modal">
<span>&times;</span>
</button>
</div>
<div class="modal-body">
<ul>
@foreach($users as $user)
    <li> <i class="fa fa-user"></i> {{ucwords($user->name)}}</li>
@endforeach

</ul>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
</div>
</div>
</div>
@endif
<!-- Fin Modal --->

@endsection
