<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth routes
Auth::routes();

/*============== Home ============== */
Route::get('/home', 'HomeController@index')->name('home');


/*============== Medicin add ============== */
Route::get('/home/medecin', 'MedecinController@addMedecin');
Route::Post('/home/medecin/store', 'MedecinController@store');


/*============== Client add ============== */
Route::get('/home/client', 'ClientController@addClient');
Route::Post('/home/client/store', 'ClientController@store');

/*============== Creanaeux show ============== */
Route::Post('/home/medecin/creneaux', 'CreneauController@showCreneauxMedecin');
Route::Get('/home/medecin/creneaux', 'CreneauController@showCreneauxMedecin');

/*============== rdv delete ============== */
Route::get('/home/rdv/{id}/{date}/delete', 'RdvController@rdvDelete');

/*============== Soap Test ============== */
Route::get('/home/soap/test', 'SoapController@soapTest');
Route::POST('/home/rdv/soap/store', 'SoapServiceController@rdvStore');
Route::Delete('/home/rdv/soap/store', 'SoapClientController@rdvDelete');


/*============== rdv create ============== */
Route::get('/home/rdv/{id}/{date}/create', 'RdvController@rdvCreate');
Route::POST('/home/rdv/store', 'RdvController@rdvStore');






