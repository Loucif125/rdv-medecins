<?php

namespace App\Http\Controllers;


use App\Creneau;
use App\Medecin;
use App\Rdv;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CreneauController extends Controller{

    public function showCreneauxMedecin(Request $request){

        // ID Medecin
        $id_medecin = $request->input('id_medecin');
        $date = $request->input('date');
        // Object Medecin
        $medecin = Medecin::find($id_medecin);
        // Creanaux
        $creneaux = Creneau::where('ID_MEDECIN', '=',$id_medecin)->get();
        // RDV
        $rdvs =Rdv::All();

        return view('home',[

            'creneaux' => $creneaux,
            'medecin' => $medecin,
            'rdvs' => $rdvs,
            'date' => $date
        ]);

    }
}
