<?php

namespace App\Http\Controllers;
use App\Creneau;
use App\Medecin;
use App\User;
use App\Client;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index(){

        $medecins = Medecin::all();
        $nbUser = count(User::all());
        $nbMedecin = count(Medecin::all());
        $nbClient = count(Client::all()) ;
        $users = User::all();

        return view('home',[

            'medecins' => $medecins,
            'nbUser'=>$nbUser,
            'nbClient' => $nbClient,
            'nbMedecin'=> $nbMedecin,
            'users' => $users


        ]);

    }


}
