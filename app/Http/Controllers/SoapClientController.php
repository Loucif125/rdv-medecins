<?php

namespace App\Http\Controllers;

use App\Creneau;
use App\Medecin;
use App\Rdv;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SoapClient;

class SoapClientController extends Controller
{
    public function __construct()
    {
        $params = array(
            'location' =>'http://127.0.0.1:8000/home/rdv/store',
            'uri' => 'http://127.0.0.1:8000/home/rdv/store',
            'trace' => 1
        );

        $this->instance = new SoapClient(NULL,$params);
    }

    public function rdvStore(Request $request){

        $this->validate($request,[
            'id_client'=>'required|unique:rv',
            'id_creneau'=>'required'
        ]);

        // NEw object Rdv
        $rdvNew = new Rdv();
        $rdvNew->JOUR = $request->input('date');
        $rdvNew->ID_CLIENT = $request->input('id_client');
        $rdvNew->ID_CRENEAU = $request->input('id_creneau');
        $rdvNew->save();

        // ID Medecin
        $id_medecin = $request->input('id_medecin');

        // Object Medecin
        $medecin = Medecin::find($id_medecin);
        // Creanaux
        $creneaux = Creneau::where('ID_MEDECIN', '=',$id_medecin)->get();
        // RDV
        $rdvs =Rdv::All();

        $date = $request->input('date');

        return view('home',[

            'creneaux' => $creneaux,
            'medecin' => $medecin,
            'rdvs' => $rdvs,
            'date'=> $date,
            'success' => 'Ajouté avec succes !'

        ]);

    }


    /*============== Rdv Delete ===============*/
    public function rdvDelete(){

        // ID Medecin
        $rdv = Rdv::where('ID',$id)->first();

        // Creneau
        $creneau = Creneau::where('ID', $rdv->ID_CRENEAU)->first();

        // Object Medecin
        $medecin = Medecin::where('ID',$creneau->ID_MEDECIN)->first();

        // Creanaux DU Medecin x
        $creneaux = Creneau::where('ID_MEDECIN', '=',$medecin->ID)->get();

        // Suppression
        DB::table('rv')->where('ID', $id)->delete();

        // RDV
        $rdvs =Rdv::All();

        return view('home',[

            'creneaux' => $creneaux,
            'medecin' => $medecin,
            'rdvs' => $rdvs,
            'date' => $date,
            'success' =>'Supprimé avec succès !'
        ]);
    }

}
