<?php

namespace App\Http\Controllers;

use http\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SoapServerController;

class SoapServiceController extends Controller
{
    public function __construct()
    {
        $client = null;
        $id_array = array('id' =>'1');

        $client->rdvStore($id_array);
        $client->rdvDelete($id_array);
    }
}
