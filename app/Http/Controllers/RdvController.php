<?php

namespace App\Http\Controllers;

use App\Client;
use App\Creneau;
use App\Medecin;
use App\Rdv;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RdvController extends Controller{


    /*===============   create   =================*/
    public function rdvCreate($id, $date){

        // Creneau
        $creneau = Creneau::where('ID', $id)->first();

        // Object Medecin
        $medecin = Medecin::where('ID', $creneau->ID_MEDECIN)->first();
        //Client
        $clients = Client::all();

        return view('createRdv', [
            'creneau' => $creneau,
            'medecin' => $medecin,
            'clients' => $clients,
            'date' => $date

        ]);
    }


    /*===============   Suppression   =================*/
    public function rdvDelete($id, $date){

        // ID Medecin
        $rdv = Rdv::where('ID',$id)->first();

        // Creneau
        $creneau = Creneau::where('ID', $rdv->ID_CRENEAU)->first();

        // Object Medecin
        $medecin = Medecin::where('ID',$creneau->ID_MEDECIN)->first();

        // Creanaux DU Medecin x
        $creneaux = Creneau::where('ID_MEDECIN', '=',$medecin->ID)->get();

        // Suppression
        DB::table('rv')->where('ID', $id)->delete();

        // RDV
        $rdvs =Rdv::All();

         return view('home',[

            'creneaux' => $creneaux,
            'medecin' => $medecin,
            'rdvs' => $rdvs,
             'date' => $date,
             'success' =>'Supprimé avec succès !'
        ]);
    }

    /*===============   Store   =================*/
    public function rdvStore(Request $request){

        $this->validate($request,[
            'id_client'=>'required|unique:rv',
            'id_creneau'=>'required'
        ]);

        // NEw object Rdv
        $rdvNew = new Rdv();
        $rdvNew->JOUR = $request->input('date');
        $rdvNew->ID_CLIENT = $request->input('id_client');
        $rdvNew->ID_CRENEAU = $request->input('id_creneau');
        $rdvNew->save();

        // ID Medecin
        $id_medecin = $request->input('id_medecin');

        // Object Medecin
        $medecin = Medecin::find($id_medecin);
        // Creanaux
        $creneaux = Creneau::where('ID_MEDECIN', '=',$id_medecin)->get();
        // RDV
        $rdvs =Rdv::All();

        $date = $request->input('date');

        return view('home',[

            'creneaux' => $creneaux,
            'medecin' => $medecin,
            'rdvs' => $rdvs,
            'date'=> $date,
            'success' => 'Ajouté avec succes !'

        ]);
    }




}
