<?php

namespace App\Http\Controllers;

use App\Client;
use App\Medecin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedecinController extends Controller{


    // Show add Medecin
    public function addMedecin(){
        $nbUser = count(User::all());
        $nbMedecin = count(Medecin::all());
        $nbClient = count(Client::all()) ;

        return view('addMedecin', [
            'nbUser'=>$nbUser,
            'nbClient' => $nbClient,
            'nbMedecin'=> $nbMedecin
        ]);
    }

    // Ajout medecins
    public function store(Request $request){

        $this->validate($request,[
            'nom'=>'required',
            'prenom'=>'required'
        ]);

        $medecin = new Medecin();
        $medecin->NOM = $request->input('nom');
        $medecin->PRENOM = $request->input('prenom');
        $medecin->TITRE = $request->input('titre');
        $medecin->VERSION = 1;
        $medecin->save();

        return redirect()->back()->with('success','Ajouté avec succès !');
    }



}
