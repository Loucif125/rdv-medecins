<?php

namespace App\Http\Controllers;

use App\Client;
use App\Medecin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller{

    // Show add Client
    public function addClient(){

        $nbUser = count(User::all());
        $nbMedecin = count(Medecin::all());
        $nbClient = count(Client::all()) ;

        return view('addClient', [

            'nbUser'=>$nbUser,
            'nbClient' => $nbClient,
            'nbMedecin'=> $nbMedecin
        ]);
    }

    // Ajout Client
    public function store(Request $request){

        $this->validate($request,[
            'nom'=>'required',
            'prenom'=>'required'
        ]);

        $medecin = new Client();
        $medecin->NOM = $request->input('nom');
        $medecin->PRENOM = $request->input('prenom');
        $medecin->TITRE = $request->input('titre');
        $medecin->VERSION = 1;
        $medecin->save();

        return redirect()->back()->with('success','Ajouté avec succès !');
    }


}
