<?php

namespace App\Http\Controllers;

use App\Creneau;
use App\Medecin;
use App\Rdv;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SoapServerController extends Controller
{
    public function __construct()
    {
        $params = array('url' => 'app/SoapServerController');
        $server = new \SoapServer(NULL, $params);
        $server->setClass('SoapServerController');
        $server->handle();
    }

    public function rdvStore(Request $request){

        $this->validate($request,[
            'id_client'=>'required|unique:rv',
            'id_creneau'=>'required'
        ]);

        // NEw object Rdv
        $rdvNew = new Rdv();
        $rdvNew->JOUR = $request->input('date');
        $rdvNew->ID_CLIENT = $request->input('id_client');
        $rdvNew->ID_CRENEAU = $request->input('id_creneau');
        $rdvNew->save();

        // ID Medecin
        $id_medecin = $request->input('id_medecin');

        // Object Medecin
        $medecin = Medecin::find($id_medecin);
        // Creanaux
        $creneaux = Creneau::where('ID_MEDECIN', '=',$id_medecin)->get();
        // RDV
        $rdvs =Rdv::All();

        $date = $request->input('date');

        return view('home',[

            'creneaux' => $creneaux,
            'medecin' => $medecin,
            'rdvs' => $rdvs,
            'date'=> $date,
            'success' => 'Ajouté avec succes !'
        ]);
    }



}
