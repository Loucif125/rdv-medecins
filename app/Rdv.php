<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rdv extends Model
{
    protected $table = 'rv';

    // ID Client
    public function rdvClientName(){

        return $this->belongsTo('App\Client','ID_CLIENT');
    }
}
