<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medecin extends Model
{
    protected $fillable = [
        'ID', 'VERSION', 'TITRE','NOM','PRENOM',
    ];

    protected $table = 'medecins';
}
