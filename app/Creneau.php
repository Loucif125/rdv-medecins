<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creneau extends Model{

    protected $table = 'creneaux';

    // User function
    public function creneauMedecin(){

        return $this->belongsTo('App\Medecin','ID_MEDECIN');
    }
}
