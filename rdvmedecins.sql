-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 26 jan. 2020 à 17:38
-- Version du serveur :  5.7.24
-- Version de PHP :  7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `rdvmedecins`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` int(11) NOT NULL,
  `TITRE` varchar(5) NOT NULL,
  `NOM` varchar(30) NOT NULL,
  `PRENOM` varchar(30) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`ID`, `VERSION`, `TITRE`, `NOM`, `PRENOM`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mr', 'MARTIN', 'Jules', NULL, NULL),
(2, 1, 'Mme', 'GERMAN', 'Christine', NULL, NULL),
(3, 1, 'Mr', 'JACQUARD', 'Jules', NULL, NULL),
(4, 1, 'Melle', 'BISTROU', 'Brigitte', NULL, NULL),
(5, 1, 'Mr', 'Mickeal', 'Dave', '2020-01-23 20:37:01', '2020-01-23 20:37:01'),
(6, 1, 'Mr', 'KHABACHE', 'Loucif', '2020-01-25 21:25:08', '2020-01-25 21:25:08');

-- --------------------------------------------------------

--
-- Structure de la table `creneaux`
--

DROP TABLE IF EXISTS `creneaux`;
CREATE TABLE IF NOT EXISTS `creneaux` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` int(11) NOT NULL,
  `HDEBUT` int(11) NOT NULL,
  `MDEBUT` int(11) NOT NULL,
  `HFIN` int(11) NOT NULL,
  `MFIN` int(11) NOT NULL,
  `ID_MEDECIN` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK9BD7A197FE16862` (`ID_MEDECIN`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `creneaux`
--

INSERT INTO `creneaux` (`ID`, `VERSION`, `HDEBUT`, `MDEBUT`, `HFIN`, `MFIN`, `ID_MEDECIN`) VALUES
(1, 1, 8, 0, 8, 20, 1),
(2, 1, 8, 20, 8, 40, 1),
(3, 1, 8, 40, 9, 0, 1),
(4, 1, 9, 0, 9, 20, 1),
(5, 1, 9, 20, 9, 40, 1),
(6, 1, 9, 40, 10, 0, 1),
(7, 1, 10, 0, 10, 20, 1),
(8, 1, 10, 20, 10, 40, 1),
(9, 1, 10, 40, 11, 0, 1),
(10, 1, 11, 0, 11, 20, 1),
(11, 1, 11, 20, 11, 40, 1),
(12, 1, 11, 40, 12, 0, 1),
(13, 1, 14, 0, 14, 20, 1),
(14, 1, 14, 20, 14, 40, 1),
(15, 1, 14, 40, 15, 0, 1),
(16, 1, 15, 0, 15, 20, 1),
(17, 1, 15, 20, 15, 40, 1),
(18, 1, 15, 40, 16, 0, 1),
(19, 1, 16, 0, 16, 20, 1),
(20, 1, 16, 20, 16, 40, 1),
(21, 1, 16, 40, 17, 0, 1),
(22, 1, 17, 0, 17, 20, 1),
(23, 1, 17, 20, 17, 40, 1),
(24, 1, 17, 40, 18, 0, 1),
(25, 1, 8, 0, 8, 20, 2),
(26, 1, 8, 20, 8, 40, 2),
(27, 1, 8, 40, 9, 0, 2),
(28, 1, 9, 0, 9, 20, 2),
(29, 1, 9, 20, 9, 40, 2),
(30, 1, 9, 40, 10, 0, 2),
(31, 1, 10, 0, 10, 20, 2),
(32, 1, 10, 20, 10, 40, 2),
(33, 1, 10, 40, 12, 0, 2),
(34, 1, 12, 0, 12, 20, 2),
(35, 1, 12, 20, 12, 40, 2),
(36, 1, 12, 40, 12, 0, 2),
(37, 1, 8, 0, 8, 20, 3),
(38, 1, 8, 20, 8, 40, 3),
(39, 1, 8, 40, 9, 0, 3),
(40, 1, 9, 0, 9, 20, 3),
(41, 1, 9, 20, 9, 40, 3),
(42, 1, 9, 40, 10, 0, 3),
(43, 1, 10, 0, 10, 20, 3),
(44, 1, 10, 20, 10, 40, 3),
(45, 1, 10, 40, 12, 0, 3),
(46, 1, 12, 0, 12, 20, 3);

-- --------------------------------------------------------

--
-- Structure de la table `medecins`
--

DROP TABLE IF EXISTS `medecins`;
CREATE TABLE IF NOT EXISTS `medecins` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` int(11) NOT NULL,
  `TITRE` varchar(5) NOT NULL,
  `NOM` varchar(30) NOT NULL,
  `PRENOM` varchar(30) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `medecins`
--

INSERT INTO `medecins` (`ID`, `VERSION`, `TITRE`, `NOM`, `PRENOM`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mme', 'PELISSIER', 'Marie', NULL, NULL),
(2, 1, 'Mr', 'BROMARD', 'Jacques', NULL, NULL),
(3, 1, 'Mr', 'JANDOT', 'Philippe', NULL, NULL),
(4, 1, 'Melle', 'JACQUEMOT', 'Justine', NULL, NULL),
(5, 1, 'Mr', 'Paster', 'PasterEssaie', '2020-01-23 20:20:21', '2020-01-23 20:20:21');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `rv`
--

DROP TABLE IF EXISTS `rv`;
CREATE TABLE IF NOT EXISTS `rv` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `JOUR` date NOT NULL,
  `ID_CLIENT` bigint(20) NOT NULL,
  `ID_CRENEAU` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNQ1_RV` (`JOUR`,`ID_CRENEAU`),
  KEY `FKA4494D97AD2` (`ID_CLIENT`),
  KEY `FKA441A673246` (`ID_CRENEAU`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rv`
--

INSERT INTO `rv` (`ID`, `JOUR`, `ID_CLIENT`, `ID_CRENEAU`, `created_at`, `updated_at`) VALUES
(11, '2020-01-25', 3, 20, NULL, NULL),
(16, '2020-02-25', 1, 3, '2020-01-25 21:00:26', '2020-01-25 21:00:26'),
(17, '2020-01-30', 5, 6, '2020-01-25 21:06:25', '2020-01-25 21:06:25'),
(22, '2020-02-26', 6, 25, '2020-01-26 15:43:56', '2020-01-26 15:43:56'),
(23, '2020-02-27', 4, 1, '2020-01-26 16:30:50', '2020-01-26 16:30:50');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `admin`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrateur', 'admin@admin.fr', 1, NULL, '$2y$10$gj0UhQ3Nslq5u429Kx4Q0uWqv2788b3dkHyaPHCg2oUD7OoziBA0C', NULL, '2020-01-23 18:00:37', '2020-01-23 18:00:37'),
(2, 'medecin', 'medecin@medecin.fr', 0, NULL, '$2y$10$s1Tb8Yrc/J8FsfWf9lT6cu1iby5TgKVjQ2HQqHBPcnAz/VUcLqV7m', NULL, '2020-01-25 15:22:22', '2020-01-25 15:22:22');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `creneaux`
--
ALTER TABLE `creneaux`
  ADD CONSTRAINT `FK9BD7A197FE16862` FOREIGN KEY (`ID_MEDECIN`) REFERENCES `medecins` (`ID`);

--
-- Contraintes pour la table `rv`
--
ALTER TABLE `rv`
  ADD CONSTRAINT `FKA441A673246` FOREIGN KEY (`ID_CRENEAU`) REFERENCES `creneaux` (`ID`),
  ADD CONSTRAINT `FKA4494D97AD2` FOREIGN KEY (`ID_CLIENT`) REFERENCES `clients` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
